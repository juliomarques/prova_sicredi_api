package main.java;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;

import static org.hamcrest.Matchers.is;
import static org.apache.http.HttpStatus.*;

import org.testng.annotations.Test;

public class RestricoesTest extends BaseTest {
    @Feature("Consulta Restrições")


    @Test
    @Story("Consultar cpf com restrição")
    public void consultarCpfComRestricao() {
        String cpf = "97093236014";
        request().get(pathRestricoes + cpf)
                .then()
                .statusCode(SC_OK)
                .body("mensagem", is("O CPF " + cpf + " possui restrição"));
    }

    @Test
    @Story("consultar cpf sem restricao")
    public void consultarCpfSemRestricao() {
        String cpf = "12345678922";
        request().get(pathRestricoes + cpf)
                .then()
                .statusCode(SC_NO_CONTENT);
    }
}
