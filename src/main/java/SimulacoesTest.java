package main.java;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.testng.annotations.Test;

import static org.apache.http.HttpStatus.*;
import static org.hamcrest.Matchers.is;

public class SimulacoesTest extends BaseTest {
    @Feature("Simulações")


    @Test
    @Story("Cadastrar simulacão com  sucesso")
    public void cadastraSimulacao() {
        String requestParams = "{\n" +
                "    \"nome\": \"Maria da Silva\",\n" +
                "    \"cpf\": \"11122233342\",\n" +
                "    \"email\": \"maria@gmail.com\",\n" +
                "    \"valor\": 20000,\n" +
                "    \"parcelas\": 5,\n" +
                "    \"seguro\": false\n" +
                "}";

        requestComBody(requestParams)
                .post(pathSimulacoes)
                .then()
                .statusCode(SC_CREATED)
                .body("nome", is("Maria da Silva"))
                .body("cpf", is("11122233342"))
                .body("email", is("maria@gmail.com"))
                .body("valor", is(20000))
                .body("parcelas", is(5))
                .body("seguro", is(false));

    }

    @Test
    @Story("Cadastrar simulacão sem nome")
    public void cadastraSimulacaoSemNome() {
        String requestParams = "{\n" +
                "    \"cpf\": \"45612378988\",\n" +
                "    \"email\": \"maria@gmail.com\",\n" +
                "    \"valor\": 20000,\n" +
                "    \"parcelas\": 5,\n" +
                "    \"seguro\": false\n" +
                "}";

        requestComBody(requestParams)
                .post(pathSimulacoes)
                .then()
                .body("erros.nome", is("Nome não pode ser vazio"))
                .statusCode(SC_BAD_REQUEST);
    }

    @Test
    @Story("Cadastrar simulacão sem cpf")
    public void cadastraSimulacaoSemCpf() {
        String requestParams = "{\n" +
                "    \"nome\": \"Maria\",\n" +
                "    \"email\": \"maria@gmail.com\",\n" +
                "    \"valor\": 20000,\n" +
                "    \"parcelas\": 5,\n" +
                "    \"seguro\": false\n" +
                "}";

        requestComBody(requestParams)
                .post(pathSimulacoes)
                .then()
                .body("erros.cpf", is("CPF não pode ser vazio"))
                .statusCode(SC_BAD_REQUEST);
    }

    @Test
    @Story("Cadastrar simulacão com cpf invalido")
    public void cadastraSimulacaoComCpfInvalido() {
        String requestParams = "{\n" +
                "    \"nome\": \"Maria\",\n" +
                "    \"cpf\": 222222222-54,\n" +
                "    \"email\": \"maria@gmail.com\",\n" +
                "    \"valor\": 20000,\n" +
                "    \"parcelas\": 5,\n" +
                "    \"seguro\": false\n" +
                "}";

        requestComBody(requestParams)
                .post(pathSimulacoes)
                .then()
                .statusCode(SC_BAD_REQUEST);
    }

    @Test
    @Story("Cadastrar simulacão com cpf existinte")
    public void cadastraSimulacaoComCpfExistente() {
        String requestParams = "{\n" +
                "    \"nome\": \"Maria\",\n" +
                "    \"cpf\": \"66414919004\",\n" +
                "    \"email\": \"maria@gmail.com\",\n" +
                "    \"valor\": 20000,\n" +
                "    \"parcelas\": 5,\n" +
                "    \"seguro\": false\n" +
                "}";

        requestComBody(requestParams)
                .post(pathSimulacoes)
                .then()
                .body("mensagem", is("CPF duplicado"))
                .statusCode(SC_BAD_REQUEST);
    }

    @Test
    @Story("Cadastrar simulacão sem email")
    public void cadastraSimulacaoSemEmail() {
        String requestParams = "{\n" +
                "    \"nome\": \"Maria\",\n" +
                "    \"cpf\": \"45612378988\",\n" +
                "    \"valor\": 20000,\n" +
                "    \"parcelas\": 5,\n" +
                "    \"seguro\": false\n" +
                "}";

        requestComBody(requestParams)
                .post(pathSimulacoes)
                .then()
                .body("erros.email", is("E-mail não deve ser vazio"))
                .statusCode(SC_BAD_REQUEST);
    }

    @Test
    @Story("Cadastrar simulacão com email invalido")
    public void cadastraSimulacaoComEmailInvalido() {
        String requestParams = "{\n" +
                "    \"nome\": \"Maria\",\n" +
                "    \"cpf\": \"45612378988\",\n" +
                "    \"email\": \"maria\",\n" +
                "    \"valor\": 20000,\n" +
                "    \"parcelas\": 5,\n" +
                "    \"seguro\": false\n" +
                "}";

        requestComBody(requestParams)
                .post(pathSimulacoes)
                .then()
                .body("erros.email", is("não é um endereço de e-mail"))
                .statusCode(SC_BAD_REQUEST);
    }

    @Test
    @Story("Cadastrar simulacão sem valor")
    public void cadastraSimulacaoSemValor() {
        String requestParams = "{\n" +
                "    \"nome\": \"Maria\",\n" +
                "    \"cpf\": \"45612378988\",\n" +
                "    \"email\": \"maria@email.com\",\n" +
                "    \"parcelas\": 5,\n" +
                "    \"seguro\": false\n" +
                "}";

        requestComBody(requestParams)
                .post(pathSimulacoes)
                .then()
                .body("erros.valor", is("Valor não pode ser vazio"))
                .statusCode(SC_BAD_REQUEST);
    }

    @Test
    @Story("Cadastrar simulacão com valor menor que mil")
    public void cadastraSimulacaoComValorMenorQueMil() {
        String requestParams = "{\n" +
                "    \"nome\": \"Maria\",\n" +
                "    \"cpf\": \"45612378988\",\n" +
                "    \"email\": \"maria@email.com\",\n" +
                "    \"valor\": 999,\n" +
                "    \"parcelas\": 5,\n" +
                "    \"seguro\": false\n" +
                "}";

        requestComBody(requestParams)
                .post(pathSimulacoes)
                .then()
                .body("erros.valor", is("Valor deve ser maior ou igual a R$ 1.000"))
                .statusCode(SC_BAD_REQUEST);
    }

    @Test
    @Story("Cadastrar simulacão com valor maior que quarenta mil")
    public void cadastraSimulacaoComValorMaiorQueQuarentaMil() {
        String requestParams = "{\n" +
                "    \"nome\": \"Maria\",\n" +
                "    \"cpf\": \"45612378988\",\n" +
                "    \"email\": \"maria@email.com\",\n" +
                "    \"valor\": 40001,\n" +
                "    \"parcelas\": 5,\n" +
                "    \"seguro\": false\n" +
                "}";

        requestComBody(requestParams)
                .post(pathSimulacoes)
                .then()
                .body("erros.valor", is("Valor deve ser menor ou igual a R$ 40.000"))
                .statusCode(SC_BAD_REQUEST);
    }

    @Test
    @Story("Cadastrar simulacão sem parcela")
    public void cadastraSimulacaoSemParcelas() {
        String requestParams = "{\n" +
                "    \"nome\": \"Maria\",\n" +
                "    \"cpf\": \"45612378988\",\n" +
                "    \"email\": \"maria@email.com\",\n" +
                "    \"valor\": 40000,\n" +
                "    \"seguro\": false\n" +
                "}";

        requestComBody(requestParams)
                .post(pathSimulacoes)
                .then()
                .body("erros.parcelas", is("Parcelas não pode ser vazio"))
                .statusCode(SC_BAD_REQUEST);
    }

    @Test
    @Story("Cadastrar simulacão com parcela menor que dois")
    public void cadastraSimulacaoComParcelaMenorQueDois() {
        String requestParams = "{\n" +
                "    \"nome\": \"Maria\",\n" +
                "    \"cpf\": \"45612378988\",\n" +
                "    \"email\": \"maria@email.com\",\n" +
                "    \"valor\": 40000,\n" +
                "    \"parcelas\": 1,\n" +
                "    \"seguro\": false\n" +
                "}";

        requestComBody(requestParams)
                .post(pathSimulacoes)
                .then()
                .body("erros.parcelas", is("Parcelas deve ser igual ou maior que 2"))
                .statusCode(SC_BAD_REQUEST);
    }

    @Test
    @Story("Cadastrar simulacão com parcela maior que quarenta e oito")
    public void cadastraSimulacaoComParcelasMaiorQueQuarentaEOito() {
        String requestParams = "{\n" +
                "    \"nome\": \"Maria\",\n" +
                "    \"cpf\": \"45612378988\",\n" +
                "    \"email\": \"maria@email.com\",\n" +
                "    \"valor\": 40000,\n" +
                "    \"parcelas\": 49,\n" +
                "    \"seguro\": false\n" +
                "}";

        requestComBody(requestParams)
                .post(pathSimulacoes)
                .then()
                .body("erros.parcelas", is("Parcelas deve ser igual ou menor que 48"))
                .statusCode(SC_BAD_REQUEST);
    }

    @Test
    @Story("Cadastrar simulacão com atributo seguro vazio")
    public void cadastraSimulacaoComAtributoSeguroVazio() {
        String requestParams = "{\n" +
                "    \"nome\": \"Maria\",\n" +
                "    \"cpf\": \"45612378988\",\n" +
                "    \"email\": \"maria@email.com\",\n" +
                "    \"valor\": 40000,\n" +
                "    \"parcelas\": 4,\n" +
                "}";

        requestComBody(requestParams)
                .post(pathSimulacoes)
                .then()
                .statusCode(SC_BAD_REQUEST);
    }

    @Test
    @Story("alterar simulacão atributo seguro para true")
    public void alterarSimulacaoAtributoSegutoParaTrue() {
        String cpf = "11122233342";
        String requestParams = "{\n" +
                "    \"nome\": \"Maria da Silva\",\n" +
                "    \"cpf\": " + cpf + ",\n" +
                "    \"email\": \"maria@gmail.com\",\n" +
                "    \"valor\": 20000,\n" +
                "    \"parcelas\": 5,\n" +
                "    \"seguro\": true\n" +
                "}";

        requestComBody(requestParams)
                .put(pathSimulacoes + cpf)
                .then()
                .statusCode(SC_OK)
                .body("nome", is("Maria da Silva"))
                .body("cpf", is(cpf))
                .body("email", is("maria@gmail.com"))
                .body("valor", is(20000.0F))
                .body("parcelas", is(5))
                .body("seguro", is(true));
    }

    @Test
    @Story("Alterar simulacão sem nome")
    public void alterarSimulacaoSemNome() {
        String cpf = "11122233342";
        String requestParams = "{\n" +
                "    \"cpf\": " + cpf + ",\n" +
                "    \"email\": \"maria@gmail.com\",\n" +
                "    \"valor\": 20000,\n" +
                "    \"parcelas\": 5,\n" +
                "    \"seguro\": true\n" +
                "}";

        requestComBody(requestParams)
                .put(pathSimulacoes + cpf)
                .then()
                .body("erros.nome", is("Nome não pode ser vazio"))
                .statusCode(SC_BAD_REQUEST);
    }

    @Test
    @Story("Alterar simulacão sem cpf")
    public void alterarSimulacaoSemCpf() {
        String cpf = "11122233342";
        String requestParams = "{\n" +
                "    \"nome\": \"Maria da Silva\",\n" +
                "    \"email\": \"maria@gmail.com\",\n" +
                "    \"valor\": 20000,\n" +
                "    \"parcelas\": 5,\n" +
                "    \"seguro\": true\n" +
                "}";

        requestComBody(requestParams)
                .put(pathSimulacoes + cpf)
                .then()
                .body("erros.nome", is("CPF não pode ser vazio"))
                .statusCode(SC_BAD_REQUEST);
    }

    @Test
    @Story("Alterar simulacão com cpf invalido")
    public void alterarSimulacaoComCpfInvalido() {
        String cpf = "11122233342";
        String requestParams = "{\n" +
                "    \"nome\": \"Maria da Silva\",\n" +
                "    \"cpf\": 123151,\n" +
                "    \"email\": \"maria@gmail.com\",\n" +
                "    \"valor\": 20000,\n" +
                "    \"parcelas\": 5,\n" +
                "    \"seguro\": true\n" +
                "}";

        requestComBody(requestParams)
                .put(pathSimulacoes + cpf)
                .then()
                .statusCode(SC_BAD_REQUEST);
    }

    @Test
    @Story("Alterar simulacão do atributo cpf para um cpf existinte")
    public void alterarSimulacaoDoAtributoCpfParaUmCpfExistente() {
        String cpf = "11122233342";
        String requestParams = "{\n" +
                "    \"nome\": \"Maria da Silva\",\n" +
                "    \"cpf\": \"66414919004\",\n" +
                "    \"email\": \"maria@gmail.com\",\n" +
                "    \"valor\": 20000,\n" +
                "    \"parcelas\": 5,\n" +
                "    \"seguro\": true\n" +
                "}";

        requestComBody(requestParams)
                .put(pathSimulacoes + cpf)
                .then()
                .body("mensagem", is("CPF duplicado"))
                .statusCode(SC_BAD_REQUEST);
    }

    @Test
    @Story("Alterar simulacão passando cpf sem simulacao")
    public void alterarSimulacaoPassandoCpfSemSimulacao() {
        String cpf = "11111111111";
        String requestParams = "{\n" +
                "    \"nome\": \"Maria da Silva\",\n" +
                "    \"cpf\": " + cpf + ",\n" +
                "    \"email\": \"maria@gmail.com\",\n" +
                "    \"valor\": 20000,\n" +
                "    \"parcelas\": 5,\n" +
                "    \"seguro\": true\n" +
                "}";

        requestComBody(requestParams)
                .put(pathSimulacoes + cpf)
                .then()
                .body("mensagem", is("CPF " + cpf + " não encontrado"))
                .statusCode(SC_NOT_FOUND);
    }

    @Test
    @Story("Alterar simulacão sem email")
    public void alterarSimulacaoSemEmail() {
        String cpf = "11122233342";
        String requestParams = "{\n" +
                "    \"nome\": \"Maria da Silva\",\n" +
                "    \"cpf\": " + cpf + ",\n" +
                "    \"valor\": 20000,\n" +
                "    \"parcelas\": 5,\n" +
                "    \"seguro\": true\n" +
                "}";

        requestComBody(requestParams)
                .put(pathSimulacoes + cpf)
                .then()
                .body("erros.email", is("E-mail não deve ser vazio"))
                .statusCode(SC_BAD_REQUEST);
    }

    @Test
    @Story("Alterar simulacão com email invalido")
    public void alterarSimulacaoComEmailInvalido() {
        String cpf = "11122233342";
        String requestParams = "{\n" +
                "    \"nome\": \"Maria da Silva\",\n" +
                "    \"cpf\": " + cpf + ",\n" +
                "    \"email\": \"maria\",\n" +
                "    \"valor\": 20000,\n" +
                "    \"parcelas\": 5,\n" +
                "    \"seguro\": true\n" +
                "}";

        requestComBody(requestParams)
                .put(pathSimulacoes + cpf)
                .then()
                .body("erros.email", is("não é um endereço de e-mail"))
                .statusCode(SC_BAD_REQUEST);
    }

    @Test
    @Story("Alterar simulacão sem valor")
    public void alterarSimulacaoSemValor() {
        String cpf = "11122233342";
        String requestParams = "{\n" +
                "    \"nome\": \"Maria da Silva\",\n" +
                "    \"cpf\": " + cpf + ",\n" +
                "    \"email\": \"maria@email.com\",\n" +
                "    \"parcelas\": 5,\n" +
                "    \"seguro\": true\n" +
                "}";

        requestComBody(requestParams)
                .put(pathSimulacoes + cpf)
                .then()
                .body("erros.valor", is("Valor não pode ser vazio"))
                .statusCode(SC_BAD_REQUEST);
    }

    @Test
    @Story("Alterar simulacão com valor menor que mil")
    public void alterarSimulacaoComValorMenorQueMil() {
        String cpf = "11122233342";
        String requestParams = "{\n" +
                "    \"nome\": \"Maria da Silva\",\n" +
                "    \"cpf\": " + cpf + ",\n" +
                "    \"email\": \"maria@email.com\",\n" +
                "    \"valor\": 999,\n" +
                "    \"parcelas\": 5,\n" +
                "    \"seguro\": true\n" +
                "}";

        requestComBody(requestParams)
                .put(pathSimulacoes + cpf)
                .then()
                .body("erros.valor", is("Valor deve ser maior ou igual a R$ 1.000"))
                .statusCode(SC_BAD_REQUEST);
    }


    @Test
    @Story("Alterar simulacão com valor maior que quarenta mil")
    public void alterarSimulacaoComValorMaiorQueQuarentaMil() {
        String cpf = "11122233342";
        String requestParams = "{\n" +
                "    \"nome\": \"Maria da Silva\",\n" +
                "    \"cpf\": " + cpf + ",\n" +
                "    \"email\": \"maria@email.com\",\n" +
                "    \"valor\": 40001,\n" +
                "    \"parcelas\": 5,\n" +
                "    \"seguro\": true\n" +
                "}";

        requestComBody(requestParams)
                .put(pathSimulacoes + cpf)
                .then()
                .body("erros.valor", is("Valor deve ser menor ou igual a R$ 40.000"))
                .statusCode(SC_BAD_REQUEST);
    }

    @Test
    @Story("Alterar simulacão com atributo parcela vazio")
    public void alterarSimulacaoComAtributoParcelaVazio() {
        String cpf = "11122233342";
        String requestParams = "{\n" +
                "    \"nome\": \"Maria da Silva\",\n" +
                "    \"cpf\": " + cpf + ",\n" +
                "    \"email\": \"maria@email.com\",\n" +
                "    \"valor\": 40001,\n" +
                "    \"seguro\": true\n" +
                "}";

        requestComBody(requestParams)
                .put(pathSimulacoes + cpf)
                .then()
                .body("erros.parcelas", is("Parcelas não pode ser vazio"))
                .statusCode(SC_BAD_REQUEST);
    }

    @Test
    @Story("Alterar simulacão com parcela menor que dois")
    public void alterarSimulacaoComParcelasMenorQueDois() {
        String cpf = "11122233342";
        String requestParams = "{\n" +
                "    \"nome\": \"Maria da Silva\",\n" +
                "    \"cpf\": " + cpf + ",\n" +
                "    \"email\": \"maria@email.com\",\n" +
                "    \"valor\": 40001,\n" +
                "    \"parcelas\": 1,\n" +
                "    \"seguro\": true\n" +
                "}";

        requestComBody(requestParams)
                .put(pathSimulacoes + cpf)
                .then()
                .body("erros.parcelas", is("Parcelas deve ser igual ou maior que 2"))
                .statusCode(SC_BAD_REQUEST);
    }

    @Test
    @Story("Alterar simulacão com parcela maior que quarenta e oito")
    public void alterarSimulacaoComParcelasMaiorQueQuarentaEOito() {
        String cpf = "11122233342";
        String requestParams = "{\n" +
                "    \"nome\": \"Maria da Silva\",\n" +
                "    \"cpf\": " + cpf + ",\n" +
                "    \"email\": \"maria@email.com\",\n" +
                "    \"valor\": 40001,\n" +
                "    \"parcelas\": 49,\n" +
                "    \"seguro\": true\n" +
                "}";

        requestComBody(requestParams)
                .put(pathSimulacoes + cpf)
                .then()
                .body("erros.parcelas", is("Parcelas deve ser igual ou menor que 48"))
                .statusCode(SC_BAD_REQUEST);
    }

    @Test
    @Story("Alterar simulacão sem seguro")
    public void alterarSimulacaoSemSeguro() {
        String cpf = "11122233342";
        String requestParams = "{\n" +
                "    \"nome\": \"Maria da Silva\",\n" +
                "    \"cpf\": " + cpf + ",\n" +
                "    \"email\": \"maria@email.com\",\n" +
                "    \"valor\": 40001,\n" +
                "    \"parcelas\": 40,\n" +
                "}";

        requestComBody(requestParams)
                .put(pathSimulacoes + cpf)
                .then()
                .statusCode(SC_BAD_REQUEST);
    }


    @Test
    @Story("Consultar simulaçoes com sucesso")
    public void consultarSimulacoesComSucesso() {
        request()
                .get(pathSimulacoes)
                .then()
                .assertThat().statusCode(SC_OK)
                .log().all();
    }

    @Test
    @Story("validar status 204 sem simulaçoes cadstradas")
    public void validaStatus204SemSimulacoesCadastradas() {
        request()
                .get(pathSimulacoes)
                .then()
                .log().all()
                .statusCode(SC_NO_CONTENT);
    }


    @Test
    @Story("consultar simulaçao com sucesso")
    public void consultarSimulacaoPorCpf() {
        String cpf = "66414919004";
        request().get(pathSimulacoes + cpf)
                .then()
                .statusCode(SC_OK)
                .body("nome", is("Fulano"))
                .body("cpf", is(cpf))
                .body("email", is("fulano@gmail.com"))
                .body("valor", is(11000.0F))
                .body("parcelas", is(3))
                .body("seguro", is(true));
    }

    @Test
    @Story("Consultar simulaçao com cpf sem simulação")
    public void consultarSimulacaoComCpfSemSimulacao() {
        String cpf = "00011100022";
        request().get(pathSimulacoes + cpf)
                .then()
                .statusCode(SC_NOT_FOUND)
                .body("mensagem", is("CPF " + cpf + " não encontrado"));

    }

    @Test
    @Story("Remover simulaçao com sucesso")
    public void removeSimulacaoComSucesso() {
        int id = 11;
        request().delete(pathSimulacoes + id)
                .then()
                .statusCode(SC_OK)
                .log().all();
    }

    @Test
    @Story("Remover simulaçao passando id inexistente")
    public void removeSimulacaoPassandoIdInexistente() {
        int id = 2;
        request().delete(pathSimulacoes + id)
                .then()
                .statusCode(SC_NOT_FOUND)
                .body("mensagem", is("Simulação não encontrada"));
    }
}
