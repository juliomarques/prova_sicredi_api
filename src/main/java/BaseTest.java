package main.java;

import io.restassured.specification.RequestSpecification;

import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;

public abstract class BaseTest {
    protected final static String baseURL = "http://localhost:";
    protected final static int port = 8080;
    protected final static String basePath = "/api";
    protected final static String pathSimulacoes = baseURL + port + basePath + "/v1/simulacoes/";
    protected final static String pathRestricoes = baseURL + port + basePath + "/v1/restricoes/";

    protected RequestSpecification requestComBody(String requestBody) {
        return given()
                .contentType(JSON)
                .body(requestBody)
                .when();
    }

    protected RequestSpecification request() {
        return given()
                .contentType(JSON)
                .when();
    }
}
