# Desafio automação de testes - API

Este é o projeto de automação de testes da prova técnica API SICREDI.

### Pre-Requisitos
* Java 8+ JDK deve estar instalado;
* Maven deve estar instalado e configurado no path da aplicação;
* Back-end esteja up
---

### Cénários de testes
* Os cenários de teste realizados no projeto estão no arquivo 'cenário de teste.md' que se encontra na raiz do projeto;

### Relação de bugs encontrados
* Foram encontrados uma relação de bugs durante a realização dos testes. A relação de bugs se encontra no arquivo 'BugReport.md' na raiz do projeto.

