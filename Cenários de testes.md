##Teste funcionais

###Endpoint Restrições
* Consultar cpf com restrição
* consultar cpf sem restricao


###Endpoint Simulações
POST:

* Cadastrar simulacão com  sucesso
* Cadastrar simulacão sem nome
* Cadastrar simulacão sem cpf				
* Cadastrar simulacão com cpf invalido			
* Cadastrar simulacão com cpf existinte				
* Cadastrar simulacão sem email			
* Cadastrar simulacão com email invalido		
* Cadastrar simulacão sem valor			
* Cadastrar simulacão com valor menor que mil			
* Cadastrar simulacão com valor maior que quarenta mil			
* Cadastrar simulacão sem parcela			
* Cadastrar simulacão com parcela menor que dois			
* Cadastrar simulacão com parcela maior que quarenta e oito		
* Cadastrar simulacão com atributo seguro vazio		


PUT:
* alterar simulacão atributo seguro para true
* Alterar simulacão sem nome
* Alterar simulacão sem cpf
* Alterar simulacão com cpf invalido
* Alterar simulacão do atributo cpf para um cpf existinte
* Alterar simulacão passando cpf sem simulacao
* Alterar simulacão sem email
* Alterar simulacão com email invalido
* Alterar simulacão sem valor
* Alterar simulacão com valor menor que mil
* Alterar simulacão com valor maior que quarenta mil
* Alterar simulacão com atributo parcela vazio
* Alterar simulacão com parcela menor que dois
* Alterar simulacão com parcela maior que quarenta e oito
* Alterar simulacão sem seguro

GET:
* Consultar simulaçoes com sucesso
* validar status 204 sem simulaçoes cadstradas
* consultar simulaçao com sucesso
* Consultar simulaçao com cpf sem simulação

DELETE:
* Remover simulaçao com sucesso
* Remover simulaçao passando id inexistente
