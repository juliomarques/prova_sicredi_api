# Bug Report
Relatório de bugs encontrados na API

##Endpoint Restições
1. **Consultar restrição com CPF com restrição está retornando mensagem "O CPF XXX tem problema" - Esperado era "O CPF XXX possui restrição"**

##Endpoint Simulações
####POST
 

1. **Cadastrar simulação para um CPF existente está retornando um HTTP Status 400 - Esperado era HTTP Status 409**

2. **Cadastrar simulação com um CPF invalido está retornando um HTTP Status 201**

3. **Cadastrar simulação com um valor menor que o permitido está retornando um HTTP Status 201 - Espearado era HTTP Status 400**

####GET
1. **Consultar todas as simulações  quando não tem simulações cadastrada esta retornando HTTP status 200 - Esperado era HTTP Status 204 **
####PUT

1. **Alterar uma simulação não está alterando o campo Valor da simulação**
2. **Alterar Simulação do atrubuto CPF para um cpf inválido está sendo permitido e retornando um HTTP Status 200**
3. **Alterar Simulação com campos 'nome', 'cpf', 'email', 'valor' e 'parcelas' vazios esta retornando HTTP Status 200**

####DELETE
1. **Deletar uma simulação está retornando HTTP Status 200 - Esperado era HTTP Status 204**
1. **Deletar uma simulação que não existe está retornando HTTP Status 200 - Esperado era HTTP Status 404 com mensagem "Simulação não encontrada"**